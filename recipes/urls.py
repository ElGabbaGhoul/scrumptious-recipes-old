from django.urls import path

from recipes.views import (
    log_rating,
    RecipeListView,
    RecipeDeleteView,
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDetailView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete")
]

# If we converted all of the create, 
# change, show, and list recipes function 
# views with views that are classes, this is
# what it would look like. Compare and contrast 
# the two code snippets.

#from django.urls import path

# from recipes.views import (
#     RecipeCreateView,
#     RecipeUpdateView,
#     RecipeDetailView,
#     RecipeListView,
# )

# urlpatterns = [
#     path("", RecipeListView.as_view(), name="recipes_list"),
#     path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
#     path("new/", RecipeCreateView.as_view(), name="recipe_new"),
#     path("edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
# ]